public class CalculatorLauncher {
    public static void main(String[] args) {

        int x = 10;
        int y = 2;

        Calculator calculator = new Calculator();

        System.out.printf("Add operation: %d + %d = %d\n", x, y, calculator.add(x, y));
        System.out.printf("Subtract operation: %d - %d = %d\n", x, y, calculator.subtract(x, y));
        System.out.printf("Multiply operation: %d * %d = %d\n", x, y, calculator.multiply(x, y));
        System.out.printf("Divide operation: %d / %d = %d\n", x, y, calculator.divide(x, y));
        try {
            System.out.printf("Divide operation with zero: %d / %d = %d\n", x, y, calculator.divide(x, 0));
        } catch (ArithmeticException e) {
            System.out.println("Cannot divide by zero!");
        }
        System.out.printf("Power operation: %d ^ %d = %d\n", x, y, calculator.power(x, y));
    }
}
