public class Calculator {

    public Calculator() {
    }

    /**
     *
     * @param x component of the sum
     * @param y component of the sum
     * @return sum of components
     */
    public int add(int x, int y) {
        return x + y;
    }

    /**
     *
     * @param x number to subtract from
     * @param y number that is subtracted
     * @return result of subtraction
     */
    public int subtract(int x, int y) {
        return x - y;
    }

    /**
     *
     * @param x multiplication factor
     * @param y multiplication factor
     * @return result of multiplication
     */
    public int multiply(int x, int y) {
        return x * y;
    }

    /**
     *
     * @param x dividend
     * @param y divisor
     * @return result of division (quotient)
     */
    public int divide(int x, int y) {
        return x / y;
    }

    /**
     *
     * @param x base
     * @param y exponent
     * @return base raised to an exponent
     */
    public int power(int x, int y) {
        return (int) Math.pow(x, y);
    }
}
